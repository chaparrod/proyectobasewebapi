﻿using AutoMapper;
using Roojo.Entities.Model;
using Roojo.WebApi.Models.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Roojo.WebApi.Helper
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize((config) =>
            {
                config.CreateMap<tblUsers, UserModel>();

                config.CreateMap<UserModel, tblUsers>();
            });
        }
    }
}