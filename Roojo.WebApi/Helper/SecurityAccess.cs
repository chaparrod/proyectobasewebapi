﻿using Newtonsoft.Json;
using Roojo.BussinesLogic.Security;
using Roojo.WebApi.Models;
using Roojo.WebApi.Models.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;

namespace Roojo.WebApi.Helper
{
    internal class SecurityAccess
    {
        internal static HeaderToken GenerateAccessToken(UserModel user)
        {
            return Task.Run(async () =>
            {
                using (UserBussinesLogic userBussinesLogic = new UserBussinesLogic())
                {
                    HeaderToken userToken = new HeaderToken
                    {
                        User = user,
                        ExpirationDate = DateTime.Now.AddDays(6), // TODO: configure this expirationdate
                        IsValid = true,
                    };
                    userToken.Payload = JsonWebToken.Encode(userToken);
                   await userBussinesLogic.UpdateUserToken(user.iIDUser, userToken.Payload);
                    return userToken;
                }
            }).GetAwaiter().GetResult();
        }

        internal static HeaderToken GenerateChangePasswordToken(UserModel user)
        {
            return Task.Run(async () =>
            {
                using (UserBussinesLogic userBussinesLogic = new UserBussinesLogic())
                {
                    HeaderToken userToken = new HeaderToken
                    {
                        User = user,
                        ExpirationDate = DateTime.Now.AddDays(6), // TODO: configure this expirationdate
                        IsValid = true,
                    };
                    userToken.Payload = JsonWebToken.Encode(userToken);

                    await userBussinesLogic.UpdateForgetPassToken(user.iIDUser, userToken.Payload);
                    return userToken;
                }
            }).GetAwaiter().GetResult();
        }

        public static HeaderToken ValidateAccessToken(string token)
        {
            return Task.Run(async () =>
            {

                using (UserBussinesLogic userBussinesLogic = new UserBussinesLogic())
                {
                    var userToken = JsonWebToken.Decode(token);
                    var desToken = JsonConvert.DeserializeObject<HeaderToken>(userToken);

                    bool validate = await userBussinesLogic.ValidateUserToken(desToken.User.iIDUser, token);

                    if (validate && desToken.ExpirationDate > DateTime.Now)
                    {
                        desToken.IsValid = true;
                    }
                    else
                    {
                        desToken.IsValid = false;
                    }

                    return desToken;
                }
            }).GetAwaiter().GetResult();
        }

        public static HeaderToken ValidateChangePasswordToken(string token)
        {
            return Task.Run(async () =>
            {
                using (UserBussinesLogic userBussinesLogic = new UserBussinesLogic())
                {

                    var userToken = JsonWebToken.Decode(token);
                    var desToken = JsonConvert.DeserializeObject<HeaderToken>(userToken);

                    bool validate = await userBussinesLogic.ValidateForgetPassToken(desToken.User.iIDUser, token);

                    if (validate && desToken.ExpirationDate > DateTime.Now)
                    {
                        desToken.IsValid = true;
                    }
                    else
                    {
                        desToken.IsValid = false;
                    }

                    return desToken;
                }
            }).GetAwaiter().GetResult();
        }

        internal static HeaderToken GetPrincipal(HttpRequestContext requestContext)
        {
            var deliveryPrincipal = requestContext.Principal as UserPrincipal;
            if (deliveryPrincipal != null)
            {
                return deliveryPrincipal.UserDetails;
            }
            return null;
        }

        public static HeaderToken ParseAuthorizationHeader(HttpActionContext context)
        {
            IEnumerable<string> values = new List<string>();
            if (context.Request.Headers.TryGetValues("Authorization", out values))
            {
                var authorizationToken = Enumerable.FirstOrDefault(values);
                var token = ValidateAccessToken(authorizationToken);
                if (token.IsValid)
                {
                    var principal = new UserPrincipal(new GenericIdentity(token.User.tEmail), new string[] { }) { UserDetails = token };
                    context.RequestContext.Principal = principal;
                }
                return token;
            }
            return null;
        }

        public static HeaderToken ParseForgetPasswordHeader(HttpActionContext context)
        {
            IEnumerable<string> values = new List<string>();
            if (context.Request.Headers.TryGetValues("Authorization", out values))
            {
                var authorizationToken = Enumerable.FirstOrDefault(values);
                var token = ValidateChangePasswordToken(authorizationToken);
                if (token.IsValid)
                {
                    var principal = new UserPrincipal(new GenericIdentity(token.User.tEmail), new string[] { }) { UserDetails = token };
                    context.RequestContext.Principal = principal;
                }
                return token;
            }
            return null;
        }
    }
}