﻿using Roojo.WebApi.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Roojo.WebApi.Filters
{
    public class ForgetPasswordFilter : AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext context)
        {
            try
            {
                var token = SecurityAccess.ParseForgetPasswordHeader(context);
                if (token != null)
                {
                    return token.IsValid;
                }
                return false;

            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}