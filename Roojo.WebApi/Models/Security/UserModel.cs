﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Roojo.WebApi.Models.Security
{
    public class UserModel
    {
        public int iIDUser { get; set; }
        public string tFirstName { get; set; }
        public string tLastName { get; set; }
        public string tEmail { get; set; }
        public string tForgetPassToken { get; set; }
        public string tDeviceToken { get; set; }
        public int iAttemps { get; set; }
        public bool bBlocked { get; set; }
        public Nullable<DateTime> dtBlockedDate { get; set; }
        public DateTime dtCreatedDate { get; set; }
    }
}