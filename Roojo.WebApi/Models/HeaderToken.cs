﻿using Roojo.WebApi.Models.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Roojo.WebApi.Models
{
    public class HeaderToken
    {
        public bool IsValid { get; set; }
        public UserModel User { get; set; }
        public int Customer { get; set; }
        public int Application { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string Payload { get; set; }
    }
}