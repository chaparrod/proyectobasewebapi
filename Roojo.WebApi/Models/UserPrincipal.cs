﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace Roojo.WebApi.Models
{
    internal class UserPrincipal : GenericPrincipal
    {
        public UserPrincipal(IIdentity identity, string[] roles)
                : base(identity, roles)
        {
        }

        public HeaderToken UserDetails { get; set; }
    }
}