﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Roojo.WebApi.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Roojo.WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //Swagger config
            SwaggerConfig.Register(config);

            // Web API configuration and services



            AutoMapperConfig.Initialize();

            // Convert to camelCase (js) format
            var formatters = config.Formatters;
            var jsonFormatter = formatters.JsonFormatter;
            var settings = jsonFormatter.SerializerSettings;
            settings.Formatting = Formatting.Indented;
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
