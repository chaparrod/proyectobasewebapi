﻿using Roojo.Core;
using Roojo.DataAccess;
using Roojo.Entities.Model;
using Roojo.Transverse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roojo.BussinesLogic.Security
{
    public class UserBussinesLogic : IDisposable
    {
        bool disposed = false;

        IRepository<tblUsers> usersRepository;

        public UserBussinesLogic()
        {
            IUnitOfWork unitOfWork = FabricaIoC.Contenedor.Resolver<UnitOfWork>();
            this.usersRepository = unitOfWork.UsersRepository;
        }

        public async Task<Boolean> ValidateForgetPassToken(int iIDUser, string token)
        {
            tblUsers User = (await usersRepository.FindOne(x => x.iIDUser == iIDUser && x.tForgetPassToken == token));
            return User != null;
        }

        public async Task<Boolean> ValidateUserToken(int iIDUser, string token)
        {
            tblUsers User = (await usersRepository.FindOne(x => x.iIDUser == iIDUser && x.tDeviceToken == token));
            return User != null;
        }

        public async Task UpdateUserToken(int iIDUser, string token)
        {
            tblUsers objUser = await usersRepository.FindById(iIDUser);
            if (objUser != null)
            {
                objUser.tDeviceToken = token;
                await usersRepository.SaveChanges();
            }
        }

        public async Task UpdateForgetPassToken(int iIDUser, string token)
        {
            tblUsers objUser = await usersRepository.FindById(iIDUser);

            objUser.tForgetPassToken = token;
            await usersRepository.SaveChanges();

        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) { return; }

            if (disposing)
            {
                //usersRepository.Dispose();
                //usersProfilesRepository.Dispose();
                //profilesRepository.Dispose();
            }
            disposed = true;
        }
    }
}
