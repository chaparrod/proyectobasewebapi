﻿using Roojo.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using System.Linq.Expressions;

namespace Roojo.DataAccess
{
    public class Repository<TEntity, TContext> : IRepository<TEntity>, IDisposable
        where TEntity : class
        where TContext : DbContext
    {
        protected TContext Context;

        public Repository(DbContext dbContext)
        {
            Context = dbContext as TContext;
        }

        public virtual TEntity Create()
        {
            return Context.Set<TEntity>().Create();
        }

        public virtual TEntity Create(TEntity entity)
        {
            return Context.Set<TEntity>().Add(entity);
        }

        public virtual TEntity Update(TEntity entity)
        {
            Context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            return entity;
        }

        public virtual void Delete(long id)
        {
            var item = Context.Set<TEntity>().Find(id);
            Context.Set<TEntity>().Remove(item);
        }

        public virtual void Delete(TEntity entity)
        {
            Context.Set<TEntity>().Remove(entity);
        }

        public virtual void Delete(Expression<Func<TEntity, bool>> where)
        {
            var objects = Context.Set<TEntity>().Where(where).AsEnumerable();
            foreach (var item in objects)
            {
                Context.Set<TEntity>().Remove(item);
            }
        }

        public async virtual Task<IQueryable<TEntity>> All()
        {
            return await Task.Run(() =>
            {
                return Context.Set<TEntity>();
            });
        }

        public async virtual Task<TEntity> FindById(long id)
        {
            return await Task.Run(() =>
            {
                return Context.Set<TEntity>().Find(id);
            });
        }

        public async virtual Task<TEntity> FindOne(Expression<Func<TEntity, bool>> where = null)
        {
            return (await FindAll(where)).FirstOrDefault();
        }

        public IQueryable<T> Set<T>() where T : class
        {
            return Context.Set<T>();
        }

        public async virtual Task<IQueryable<TEntity>> FindAll(Expression<Func<TEntity, bool>> where = null)
        {
            return await Task.Run(() =>
            {
                return null != where ? Context.Set<TEntity>().Where(where) : Context.Set<TEntity>();
            });
        }

        public async virtual Task<bool> SaveChanges()
        {
            return await Task.Run(() =>
            {
                return 0 < Context.SaveChanges();
            });
        }

        /// <summary>
        /// Releases all resources used by the Entities
        /// </summary>
        public void Dispose()
        {
            if (null != Context)
            {
                Context.Dispose();
            }
        }


    }
}
