﻿using Roojo.Core;
using Roojo.Entities.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roojo.DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        private ModelEntities modelEntities = new ModelEntities();

        private IRepository<tblUsers> usersRepository;

        public IRepository<tblUsers> UsersRepository
        {
            get
            {
                if (usersRepository == null)
                {
                    usersRepository = new Repository<tblUsers, ModelEntities>(modelEntities);
                }
                return usersRepository;
            }
        }


        public void SaveChanges()
        {
            modelEntities.SaveChanges();
        } 
    }
}
