﻿using Roojo.Entities.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roojo.Core
{
    public interface IUnitOfWork
    {
        IRepository<tblUsers> UsersRepository { get; }

        void SaveChanges();
    }
}
