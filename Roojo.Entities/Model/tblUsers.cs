//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Roojo.Entities.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblUsers
    {
        public int iIDUser { get; set; }
        public string tFirstName { get; set; }
        public string tLastName { get; set; }
        public string tPassword { get; set; }
        public string tEmail { get; set; }
        public string tForgetPassToken { get; set; }
        public string tDeviceToken { get; set; }
        public int iAttemps { get; set; }
        public bool bBlocked { get; set; }
        public Nullable<System.DateTime> dtBlockedDate { get; set; }
        public System.DateTime dtCreatedDate { get; set; }
    }
}
